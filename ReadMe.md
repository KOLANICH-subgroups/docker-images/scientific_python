Scripts for building an image containing cpython and libs and tools needed for science
===================================================================

Scripts in this repo are [Unlicensed ![](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/).

Third-party components have own licenses.

**DISCLAIMER: BADGES BELOW DO NOT REFLECT THE STATE OF THE DEPENDENCIES IN THE CONTAINER**

This builds a Docker image contains the following components:
  * [`registry.gitlab.com/kolanich-subgroups/docker-images/fixed_python` Docker image](https://gitlab.com/KOLANICH-subgroups/docker-images/fixed_python/container_registry), each component of which has own license;
  * [`numpy`](https://github.com/numpy/numpy) [![PyPi Status](https://img.shields.io/pypi/v/numpy.svg)](https://pypi.python.org/pypi/numpy) [![Licence](https://img.shields.io/github/license/numpy/numpy.svg)](https://github.com/numpy/numpy/blob/master/LICENSE.txt) [![Libraries.io Status](https://img.shields.io/librariesio/github/numpy/numpy.svg)](https://libraries.io/github/numpy/numpy) from pypi 
  * [`scipy`](https://github.com/scipy/scipy) [![PyPi Status](https://img.shields.io/pypi/v/scipy.svg)](https://pypi.python.org/pypi/scipy) [![Licence](https://img.shields.io/github/license/scipy/scipy.svg)](https://github.com/scipy/scipy/blob/master/LICENSE.txt) [![Libraries.io Status](https://img.shields.io/librariesio/github/scipy/scipy.svg)](https://libraries.io/github/scipy/scipy) from pypi
  * [`scikit-learn`](https://github.com/scikit-learn/scikit-learn) [![PyPi Status](https://img.shields.io/pypi/v/scikit-learn.svg)](https://pypi.python.org/pypi/scikit-learn) [![Licence](https://img.shields.io/github/license/scikit-learn/scikit-learn.svg)](https://github.com/scikit-learn/scikit-learn/blob/master/COPYING) [![Libraries.io Status](https://img.shields.io/librariesio/github/scikit-learn/scikit-learn.svg)](https://libraries.io/github/scikit-learn/scikit-learn) from pypi
  * dependencies of everything above.
