# syntax=docker/dockerfile:experimental
FROM registry.gitlab.com/kolanich-subgroups/docker-images/fixed_python
LABEL maintainer="KOLANICH"
WORKDIR /tmp
ADD ./*.sh ./
ADD ./pythonPackagesToInstallFromGit.txt ./
RUN \
  set -ex;\
  ./installPythonPackagesFromGit.sh ;\
  pip3 install --upgrade --pre --prefer-binary --only-binary all numpy scipy scikit-learn;\
  rm -r ~/.cache/pip ;\
  rm -rf /tmp/*
